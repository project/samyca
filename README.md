# Samyca

Samyca is a **Drupal distribution** full of adaptable functionalities and
essential modules for the development of a current website, it allows
you to accelerate your development and provides you with standardized
configurations, making your life easier.

### Installation

```sh
composer require drupal/samyca
```


